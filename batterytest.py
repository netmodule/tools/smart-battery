#!/usr/bin/python3

#Import
import smbus
import logging
import time
import os
import signal
import sys, getopt
#Setting
LOG_FILENAME = 'battery.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG,format='%(asctime)s , %(message)s',datefmt='%d %b %Y, %H:%M:%S',)


bus = smbus.SMBus(0)    # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)

DEVICE_ADDRESS = 0x0b      #7 bit address (will be left shifted to add the read write bit)
DEVICE_REG_MODE1 = 0x00
DEVICE_REG_ALARMWARNING = 0x0
SLEEP_TIME =25000
import atexit

def exit_handler():
   print('My application is ending!')



def printallregister():
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x0)
   print('[ManufacturerAccess()  0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x1)
   print('[RemainingCapacityAlarm()   0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x2)
   print('[RemainingTimeAlarm()  0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x3)
   print('[BatteryMode()  0x%x, %d]' %(out_values, out_values))

   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x4)
   print('[AtRate() 0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x5)
   print('[AtRateTimeToFull() 0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x6)
   print('[AtRateTimeToEmpty()   0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x7)
   print('[AtRateOK()   0x%x, %d]' %(out_values, out_values))

   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x8)
   print('[Temperature()) 0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x9)
   print('[Voltage() 0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0xa)
   print('[Current()   0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0xb)
   print('[AverageCurrent()   0x%x, %d]' %(out_values, out_values))

   out_values =bus.read_word_data(DEVICE_ADDRESS, 0xc)
   print('[MaxError() 0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0xd)
   print('[RelativeStateOfCharge()   0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0xe)
   print('[AbsoluteStateOfCharge())   0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0xf)
   print('[RemainingCapacity()  0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x10)
   print('[FullChargeCapacity())   0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x11)
   print('[RunTimeToEmpty()  0x%x, %d]' %(out_values, out_values))

   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x12)
   print('[AverageTimeToEmpty()  0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x13)
   print('[AverageTimeToFull() 0x%x, %d]' %(out_values, out_values))

   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x14)
   print('[ChargingCurrent(0x14) 0x%x, %d]' %(out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x15)
   print('[ChargingVoltage(0x15) 0x%x, %d]' % (out_values, out_values))

   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x16)
   print('[BatteryStatus() 0x%x, %d]' % (out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x19)
   print('[DesignVoltage() 0x%x, %d]' % (out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x1c)
   print('[Serialnumber 0x%x, %d]' % (out_values, out_values))

   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x21)
   print('[SelectorState(0x21) 0x%x, %d]' % (out_values, out_values))
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x22)
   print('[SelectorPresets(0x22) 0x%x, %d]' % (out_values, out_values))
   #out_values =bus.read_word_data(DEVICE_ADDRESS, 0x24)
   #print '[SelectorINfo(0x24) 0x%x, %d]' % (out_values, out_values)

   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x3c)
   print('[OptionalMfgFunction4  0x3c 0x%x]' % out_values)
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x3d)
   print('[OptionalMfgFunction3 3d 0x%x]' % out_values)
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x3e)
   print('[OptionalMfgFunction2 3e 0x%x]' % out_values)
   out_values =bus.read_word_data(DEVICE_ADDRESS, 0x3f)
   print('[OptionalMfgFunction1 3f 0x%x]' % out_values)

def readandlog():
   count = 0
   while (count < 10):
      try:
         AbsoluteStateOfCharge =bus.read_word_data(DEVICE_ADDRESS, 0xe)
         RemainingCapacity =bus.read_word_data(DEVICE_ADDRESS, 0xf)
         Temperaturekelvin =bus.read_word_data(DEVICE_ADDRESS, 0x8)
         TemperatureCelsius= (Temperaturekelvin*0.1)- 273.15
         AverageTimeToEmpty =bus.read_word_data(DEVICE_ADDRESS, 0x12)
         AverageCurrent =bus.read_word_data(DEVICE_ADDRESS, 0xb)
         Voltage =bus.read_word_data(DEVICE_ADDRESS, 0x9)
         AtRateTimeToEmpty =bus.read_word_data(DEVICE_ADDRESS, 0x6)
         logging.debug(' %d , %d, %d, %f',AbsoluteStateOfCharge,Voltage,AtRateTimeToEmpty,TemperatureCelsius)
         break
      except IOError:
         count = count + 1
         time.sleep(1)

      #logging.debug('AbsoluteStateOfCharge [Percent], CurrentVoltage [mV], AtRateTimeToEmpty [min], temperature [celsuis]')
   if count == 10:
      logging.debug('Could not Access i2c after 10 try over 10 second')

def interfacesdown():
   os.system("ifconfig wlan0 down")
   os.system("ifconfig eth0 down")
   os.system("systemctl stop tibluetooth")
def interfacesup():
   time.sleep(5)
   os.system("ifconfig wlan0 up")
   os.system("ifconfig eth0 up")
   os.system("systemctl start tibluetooth")
   time.sleep(5)
def handler(signum, frame):
   logging.debug('SIGINT handled')
def handler2(signum, frame):
   logging.debug('SIGTSTP or SIGTERM handled')


def help_cli():
   print("battery tool")
   print(" ")
   print("Examples:")
   print("   batterytest -v         get voltage")
   print("   batterytest -a         get current")
   print("   batterytest -t         do battery test")
   print(" ")
   print(" Main modes of operation:")
   print("  -h, --help              print this help")
   print("  -A, --all               print all battery information")
   print("  -v, --voltage           get battery voltage")
   print("  -a, --current           get battery current")
   print("  -c, --capacity          get remaining capacity")
   print("  -r, --reg               raw mode, register access")
   print("  -t, --test              run battery test")	



def main(argv):
   try:
      opts, args = getopt.getopt(argv,"hAvaCr:t", ["help", "all", "voltage", "current","capacity","reg","test"])
   except getopt.GetoptError:
      help_cli()
      sys.exit(2)
   for opt, arg in opts:
      if opt in ('-h','--help'):
         help_cli()
      elif opt in ('-A','--all'):
         printallregister()
      elif opt in ('-v','--voltage'):
         out_values =bus.read_word_data(DEVICE_ADDRESS, 0x9)
         print('[Voltage() 0x%x, %d]' %(out_values, out_values))
      elif opt in ('-a','--current'):
         out_values =bus.read_word_data(DEVICE_ADDRESS, 0xa)
         print('[Current()   0x%x, %d]' %(out_values, out_values))
      elif opt in ('-c','--capacity'):
         out_values =bus.read_word_data(DEVICE_ADDRESS, 0xf)
         print('[RemainingCapacity()  0x%x, %d]' %(out_values, out_values))
      elif opt in ('-r','--reg'):
         #print(str(arg))
         raw_cmd = int(arg,0) 
         out_values =bus.read_word_data(DEVICE_ADDRESS, raw_cmd) 
         print('[SelectorState(0x%x) 0x%x, %d]' % (raw_cmd, out_values, out_values))
      elif opt in ('-t','--test'):
         #sleep
         os.system("echo 60000 > /sys/devices/platform/wakeup_timer\@0/sleep_time_ms ")
         logging.debug('New logging sequence has started')
         logging.debug('AbsoluteStateOfCharge [Percent], CurrentVoltage [mV], AtRateTimeToEmpty [min], temperature [celsuis]')
         readandlog()
         atexit.register(exit_handler)
         signal.signal(signal.SIGINT, handler)
         signal.signal(signal.SIGTSTP, handler2)
         signal.signal(signal.SIGTERM, handler2)
         while True:
            interfacesup()
            readandlog()
            time.sleep(25)
            interfacesdown()
            os.system("echo standby > /sys/power/state")

if __name__ == '__main__':
   main(sys.argv[1:])
   #logging.debug('logging sequencestooped')
   #printallregister()
